using System.Collections.Generic;
using Laboratorio5Avalonia.Models;

namespace Laboratorio5Avalonia.Services;

public class Database
{
    public IEnumerable<User> GetItems()
    {
        return new[]
        {
            new User("Vinycius", "Florêncio", "vinycius.florencio@gmail.com"),
            new User("Lucas", "Mateus", "lucas.mateus@gmail.com"),
            new User("Lim", "José ", "lim.jose@gmail.com"),
            new User("Jala", "Contact ", "contact@jala.university"),
            new User("Unique", "Admin ", "admin@jala.university"),
            new User("Sergio", "Alcantra ", "sergio.alcantra@outlook.com")
        };
    }
}