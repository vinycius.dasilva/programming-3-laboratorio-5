using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using Laboratorio5Avalonia.Services;
using Laboratorio5Avalonia.ViewModels;
using Laboratorio5Avalonia.Views;

namespace Laboratorio5Avalonia;

public class App : Application
{
    public override void Initialize()
    {
        AvaloniaXamlLoader.Load(this);
    }

    public override void OnFrameworkInitializationCompleted()
    {
        if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
        {
            var db = new Database();
            desktop.MainWindow = new MainWindow
            {
                DataContext = new MainWindowViewModel(db)
            };
        }

        base.OnFrameworkInitializationCompleted();
    }
}