using System;
using Avalonia.Controls;

namespace Laboratorio5Avalonia.Views;

public partial class MessageBox : Window
{
    private static MessageBox? _instance;

    private MessageBox(string message)
    {
        InitializeComponent();
        DataContext = this;
        MessageLabel.Text = message;
        SizeToContent = SizeToContent.WidthAndHeight;
        WindowStartupLocation = WindowStartupLocation.CenterScreen;
    }

    public static MessageBox? CreateBox(string message)
    {
        return _instance ??= new MessageBox(message);
    }

    protected override void OnClosed(EventArgs e)
    {
        _instance = null;
        base.OnClosed(e);
    }

    private void Window_Deactivated(object sender, EventArgs e)
    {
        Close();
    }
}