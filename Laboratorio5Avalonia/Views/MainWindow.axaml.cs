using Avalonia.Controls;
using Avalonia.Interactivity;
using Laboratorio5Avalonia.Services;
using Laboratorio5Avalonia.ViewModels;

namespace Laboratorio5Avalonia.Views;

public partial class MainWindow : Window
{
    private readonly MainWindowViewModel _mainWindowViewModel = new(new Database());

    public MainWindow()
    {
        InitializeComponent();
        DataContext = _mainWindowViewModel;
    }

    public void ShowRegisterWindow(object? sender, RoutedEventArgs routedEventArgs)
    {
        var registerViewWindow = new RegisterViewWindow(_mainWindowViewModel);
        registerViewWindow.Show(this);
    }

    private void RegisterUser(object? sender, RoutedEventArgs e)
    {
        if (FirstName.Text == null || Lastname.Text == null || Email.Text == null) return;
        _mainWindowViewModel.UserAdded += () =>
        {
            MessageBox.CreateBox("O novo usuário foi adicionado!" +
                                 $"\n\tNome: {FirstName.Text}\n\tSobrenome: {Lastname.Text}\n\tEmail: {Email.Text}")
                ?.Show(this);
        };
        _mainWindowViewModel.RegisterUser(FirstName.Text, Lastname.Text, Email.Text);
    }

    private void ShowErrors_Click(object sender, RoutedEventArgs e)
    {
        var errors = _mainWindowViewModel.GetErrors();
        string errorMessages = "Mensagens de erro:\n";

        foreach (var error in errors)
        {
            errorMessages += $"{error.timestamp}: {error.message}\n";
        }

        MessageBox.CreateBox(errorMessages)?.Show(this);
    }

}