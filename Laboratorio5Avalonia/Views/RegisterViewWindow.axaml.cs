using Avalonia.Controls;
using Avalonia.Interactivity;
using Laboratorio5Avalonia.ViewModels;

namespace Laboratorio5Avalonia.Views;

public partial class RegisterViewWindow : Window
{
    private readonly MainWindowViewModel _viewModel;

    public RegisterViewWindow(MainWindowViewModel windowViewModel)
    {
        _viewModel = windowViewModel;
        InitializeComponent();
        SortAscending.Click += SortAscendingClick;
        SortDescending.Click += SortDescendingClick;
        DataContext = windowViewModel;
        ListBoxUserList.ItemsSource = windowViewModel.UserList.Items;
    }

    private void SortAscendingClick(object sender, RoutedEventArgs e)
    {
        _viewModel.RegisterSorted += () =>
        {
            MessageBox.CreateBox("Processo de organização crescente foi concluída!")?.Show(this);
        };
        _viewModel.SortAscendingClick();
    }

    private void SortDescendingClick(object sender, RoutedEventArgs e)
    {
        _viewModel.RegisterSorted += () =>
        {
            MessageBox.CreateBox("Processo de organização decrescente foi concluída!")?.Show(this);
        };
        _viewModel.SortDescendingClick();
    }

    private void EmailSearch_TextChanged(object sender, TextChangedEventArgs e)
    {
        var searchText = EmailSearch.Text;
        var tempObservableCollection = _viewModel.EmailSearcherFilter(searchText);
        ListBoxUserList.ItemsSource = tempObservableCollection;
    }
}