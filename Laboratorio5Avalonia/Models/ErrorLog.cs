using System;
using System.Collections.Generic;

namespace Laboratorio5Avalonia.Models;

public class ErrorLog
{
    private readonly Stack<(string message, DateTime timestamp)> _errorStack = new();

    public void LogError(string errorMessage)
    {
        _errorStack.Push((errorMessage, DateTime.Now));
    }

    public IEnumerable<(string message, DateTime timestamp)> GetErrors()
    {
        return _errorStack;
    }
}