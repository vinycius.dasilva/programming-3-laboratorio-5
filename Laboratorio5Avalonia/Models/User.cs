using System;

namespace Laboratorio5Avalonia.Models;

public struct User(string name, string surname, string email) : IComparable<User>
{
    public string Name { get; set; } = name;
    public string Surname { get; set; } = surname;
    public string Email { get; set; } = email;

    public int CompareTo(User other)
    {
        return string.Compare(Email, other.Email, StringComparison.Ordinal);
    }

    public override string ToString()
    {
        return $"Nome: {Name}\nSobrenome: {Surname}\nEmail: {Email}";
    }
}