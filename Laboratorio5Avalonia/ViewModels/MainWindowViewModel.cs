﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Laboratorio5Avalonia.Models;
using Laboratorio5Avalonia.Services;
using ReactiveUI;

namespace Laboratorio5Avalonia.ViewModels;

public class MainWindowViewModel : ViewModelBase
{
    private ViewModelBase _content;
    public event Action<string> ErrorOccurred;

    private readonly ErrorLog _errorLog;

    public MainWindowViewModel(Database db)
    {
        _errorLog = new ErrorLog();
        Content = UserList = new UsersViewModel(db.GetItems());
    }

    public UsersViewModel UserList { get; }

    public ViewModelBase Content
    {
        get => _content;
        private set => this.RaiseAndSetIfChanged(ref _content, value);
    }

    public event Action UserAdded;
    public event Action RegisterSorted;


    public void SortAscendingClick()
    {
        try
        {
            var sortedItems = UserList.Items.OrderBy(user => user.Email).ToList();
            UserList.Items.Clear();
            foreach (var user in sortedItems) UserList.Items.Add(user);
            RegisterSorted?.Invoke();
        }
        catch (Exception ex)
        {
            _errorLog.LogError(ex.Message);
            ErrorOccurred?.Invoke(ex.Message);
        }
    }

    public void SortDescendingClick()
    {
        try
        {
            var sortedItems = UserList.Items.OrderByDescending(user => user.Email).ToList();
            UserList.Items.Clear();
            foreach (var user in sortedItems) UserList.Items.Add(user);
            RegisterSorted?.Invoke();
        }
        catch (Exception ex)
        {
            _errorLog.LogError(ex.Message);
            ErrorOccurred?.Invoke(ex.Message);
        }
    }

    public IEnumerable<(string message, DateTime timestamp)> GetErrors()
    {
        return _errorLog.GetErrors();
    }

    public ObservableCollection<User> EmailSearcherFilter(string? searchText)
    {
        var filteredUsers = UserList.Items.Where(
            user =>
                searchText != null &&
                user.Email.Contains(searchText, StringComparison.OrdinalIgnoreCase)
        ).ToList();
        return new ObservableCollection<User>(filteredUsers);
    }

    public void RegisterUser(string firstName, string lastName, string email)
    {
        UserList.Items.Add(new User(firstName, lastName, email));
        UserAdded?.Invoke();
    }
}