using System.Collections.Generic;
using System.Collections.ObjectModel;
using Laboratorio5Avalonia.Models;

namespace Laboratorio5Avalonia.ViewModels;

public class UsersViewModel : ViewModelBase
{
    public UsersViewModel(IEnumerable<User> users)
    {
        Items = new ObservableCollection<User>(users);
    }

    public ObservableCollection<User> Items { get; }
}